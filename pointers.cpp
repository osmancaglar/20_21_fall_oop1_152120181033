#include <iostream>
#include<stdlib.h>

using namespace std;

void update(int *a, int *b) {
	int olda, oldb;
	olda = *a;///We took a backup so that the previous version and the new version are not confused.
	oldb = *b;
	if ((*a) > (*b)) {
		*a = (olda)+(oldb);
		*b = (olda)-(oldb);
	}
	else
	{
		*a = (olda)+(oldb);
		*b = (oldb)-(olda);
	}
}

int main() {
	int a, b;
	int *pa = &a, *pb = &b;

	scanf_s("%d %d", &a, &b);
	update(pa, pb);
	printf("%d\n%d", a, b);

	system("pause");
}