//#include<bits/stdc++.h>
#include<iostream>
#include<stdlib.h>

using namespace std;
//Implement the class Box  
//l,b,h are integers representing the dimensions of the box
class Box {
private:
	int l, b, h;

public:
	Box() {///defalut constructor
		l = 0;
		b = 0;
		h = 0;
	}
	Box(int newl, int newb, int newh) {///parametrized constructor
		l = newl;
		b = newb;
		h = newh;
	}

	Box(const Box &copiedBox) {///copy constructor
		l = copiedBox.l;
		b = copiedBox.b;
		h = copiedBox.h;
	}

	bool operator<(Box &comparedBox) {///function comparing 2 objects
		if (l < comparedBox.l) {
			return true;
		}
		else if (b < comparedBox.b && l == comparedBox.l) {
			return true;
		}
		else if (h < comparedBox.h && b == comparedBox.b && l == comparedBox.l) {
			return true;
		}
		else
			return false;
	}


	int getLength() { return l; }///functions to get values
	int getBreadth() { return b; }
	int getHeight() { return h; }
	unsigned long long CalculateVolume() { return (long long)l*b*h; }

};

ostream& operator<<(ostream& out, Box& B) {///operator overloading
	cout << B.getLength() << " " << B.getBreadth() << " " << B.getHeight();
	return out;
};
// The class should have the following functions : 

// Constructors: 
// Box();
// Box(int,int,int);
// Box(Box);


// int getLength(); // Return box's length
// int getBreadth (); // Return box's breadth
// int getHeight ();  //Return box's height
// long long CalculateVolume(); // Return the volume of the box

//Overload operator < as specified
//bool operator<(Box& b)

//Overload operator << as specified
//ostream& operator<<(ostream& out, Box& B)


void check2()
{
	int n;
	cin >> n;
	Box temp;
	for (int i = 0; i < n; i++)
	{
		int type;
		cin >> type;
		if (type == 1)
		{
			cout << temp << endl;
		}
		if (type == 2)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			temp = NewBox;
			cout << temp << endl;
		}
		if (type == 3)
		{
			int l, b, h;
			cin >> l >> b >> h;
			Box NewBox(l, b, h);
			if (NewBox < temp)
			{
				cout << "Lesser\n";
			}
			else
			{
				cout << "Greater\n";
			}
		}
		if (type == 4)
		{
			cout << temp.CalculateVolume() << endl;
		}
		if (type == 5)
		{
			Box NewBox(temp);
			cout << NewBox << endl;
		}

	}
}

int main()
{
	check2();
}