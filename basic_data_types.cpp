#include<stdio.h>
#include <stdlib.h>

using namespace std;

int main() {
	int integerNum;
	long longNum;
	char ch;
	float floatNum;
	double doubleNum;


	scanf_s("%d %ld %c %f %lf", &integerNum, &longNum, &ch, &floatNum, &doubleNum);

	printf("%d\n%ld\n%c\n%f\n%lf", integerNum, longNum, ch, floatNum, doubleNum);

	return 0;
}