#include<iostream>
#include<stdlib.h>
#include<fstream>
#include<string>

#define SIZE 100 /** In this section I determined the size of the matrix that I should use. */

using namespace std;

int readFile(string, float[SIZE]); /** readFile: Opens the input file and starts the reading process. */
float findSumOfNumbers(float[SIZE], int); /** findSumOfNumbers: It is the function where the read numbers are added together. */
void findProductOfNumbers(float[SIZE], int); /** findProductOfNumbers: It is the function that allows the numbers read to be multiplied with each other. */
void findAverageOfNumbers(int, float); /** findAverageOfNumbers: It is the function that allows finding the average of the numbers read. */
void findSmallestNumber(float[SIZE], int); /** findSmallestNumber: It is a function that is used to find the number with the smallest value among the numbers read. */

int main() {

	/** The part where the variables I will use are defined. */
	float arrayOfNumbers[SIZE], sumOfNumbers;
	int howManyNumbers, i;

	/** In this section, functions are called. */
	howManyNumbers = readFile("input.txt", arrayOfNumbers);

	sumOfNumbers = findSumOfNumbers(arrayOfNumbers, howManyNumbers);
	findProductOfNumbers(arrayOfNumbers, howManyNumbers);
	findAverageOfNumbers(howManyNumbers, sumOfNumbers);
	findSmallestNumber(arrayOfNumbers, howManyNumbers);

	cout << "\n\n";
	system("pause");
}

// The part where the definitions of functions are made.
int readFile(string fileName, float arrayOfNumbers[SIZE]) {

	int howManyNumbers, i;
	fstream inputFile;

	inputFile.open(fileName, ios::in); /** Opens the file in read mode. */

	if (!inputFile)
	{
		cout << "The file was not opened!\n\n";
		exit(0);
	}
	else
		cout << "The file was opened successfully. File reading process started.\n\n";

	inputFile >> howManyNumbers;

	i = 0;
	while (!inputFile.eof()) { /** Assigns numbers read from the file to an array. */
		inputFile >> arrayOfNumbers[i];
		i++;
	}

	return howManyNumbers;
}

float findSumOfNumbers(float arrayOfNumbers[SIZE], int howManyNumbers) {
	float sumOfNumbers = 0.0;
	int i;
	for (i = 0; i < howManyNumbers; i++) /** It is a loop that traverses all the elements in the array and adds them to the variable "sum". */
		sumOfNumbers += arrayOfNumbers[i];

	cout << "Sum is: " << sumOfNumbers << endl;

	return sumOfNumbers;
}
void findProductOfNumbers(float arrayOfNumbers[SIZE], int howManyNumbers) {
	float productOfNumbers = 1;
	int i;
	for (i = 0; i < howManyNumbers; i++) /** A loop that traverses all the elements in the array and sequentially multiplies them with the variable productOfNumbers. */
		productOfNumbers *= arrayOfNumbers[i];

	cout << "Product is: " << productOfNumbers << endl;
}
void findAverageOfNumbers(int howManyNumbers, float sumOfNumbers) {
	float averageOfNumbers;

	averageOfNumbers = sumOfNumbers / howManyNumbers; /** It calls the "findSumOfNumbers" function and divides it by the number of elements. */

	cout << "Average is: " << averageOfNumbers << endl;
}
void findSmallestNumber(float arrayOfNumbers[SIZE], int howManyNumbers) {
	int i;
	float smallestNumber;

	smallestNumber = arrayOfNumbers[0];

	for (i = 0; i < howManyNumbers; i++) { /** It is a loop to find the minimum by comparing the elements in the array with other elements. The loop traverses all the elements in the array, respectively, and makes comparisons through the if block. */
		if (arrayOfNumbers[i] < smallestNumber)
			smallestNumber = arrayOfNumbers[i];
	}

	cout << "Smallest number is: " << smallestNumber << endl;
}
