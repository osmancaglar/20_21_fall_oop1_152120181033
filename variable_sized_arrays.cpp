#include <iostream>
#include <stdlib.h>

using namespace std;


int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */
	int n, q, rowNumber, colNumber;

	cin >> n >> q; //n: how many arrays, q: how many queries

	int** matrix = new int*[n];///We dynamically created the rows of the matrix according to the dimensions entered.


	int temp;
	for (int i = 0; i < n; i++) {
		cin >> temp;
		matrix[i] = new int[temp];///We dynamically created the columns of the matrix according to the dimensions entered.
		for (int j = 0; j < temp; j++) {
			cin >> matrix[i][j];
		}
	}

	int tempI, tempJ;
	for (int i = 0; i < q; i++) {///We put the entered numbers in the matrix
		cin >> tempI;
		cin >> tempJ;
		cout << matrix[tempI][tempJ] << endl;
	}








	return 0;
}