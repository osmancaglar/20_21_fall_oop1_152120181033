#include<iostream>
#include<stdlib.h>

using namespace std;

int max_of_four(int, int, int, int);

int main() {
	int a, b, c, d;

	cin >> a >> b >> c >> d;

	cout <<max_of_four(a, b, c, d) << endl;
	

	system("pause");
}

int max_of_four(int a, int b, int c, int d) {///First it compares the 1st number with the 2nd number and then the 3rd number and the 4th number. it then compares the number from the first result with the number from the second result.
	int max1, max2, max3;
	max1 = a;
	max2 = c;

	if (b > a)
		max1 = b;
	if (d > c)
		max2 = d;

	if (max1 > max2)
		max3 = max1;
	else
		max3 = max2;

	return max3;
}