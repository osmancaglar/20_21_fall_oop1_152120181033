#include <iostream>
#include<stdlib.h>
#include <sstream>
#include <vector>
#include<math.h>

using namespace std;

vector<int> parseInts(string str) {
	// Complete this function
	vector <int> tempVector;
	stringstream tempStr(str);
	char ch;
	int tempInt;

	if (tempStr.str().size() > 8 * pow(10, 5)) {
		cout << "The length of  is not less than 8x(10^5)." << endl;
		exit(0);
	}
	while (true) {
		tempStr >> tempInt;

		tempVector.push_back(tempInt);

		tempStr >> ch;///Thanks to the ch variable, we have skipped commas.
		if (tempStr.eof()) {
			break;
		}
	}

	return tempVector;
}

int main() {
	string str;
	cin >> str;
	vector<int> integers = parseInts(str);
	for (int i = 0; i < integers.size(); i++) {
		cout << integers[i] << "\n";
	}

	return 0;
}

