#include <iostream>
#include <stdlib.h>
#include <string>
#include <sstream>

using namespace std;

/*
Enter code for class Student here.
Read statement for specification.
*/
class Student {
private:
	string first_name, last_name;
	int age, standard;
	string strAge, strStandard;


public:

	void set_age(int tempAge) {///set functions
		age = tempAge;
		strAge = to_string(tempAge);
	}

	void set_standard(int tempStandard) {
		standard = tempStandard;
		strStandard = to_string(tempStandard);
	}

	void set_first_name(string tempFirstName) {
		first_name = tempFirstName;
	}

	void set_last_name(string tempLastName) {
		last_name = tempLastName;
	}
	int get_age() {///get functions
		return age;
	}

	int get_standard() {
		return standard;
	}

	string get_first_name() {
		return first_name;
	}

	string get_last_name() {
		return last_name;
	}

	int a;
	
	string to_string() {///printing all members of student class
		string temp = strAge + "," + first_name + "," + last_name + "," + strStandard;
		return temp;
	}

};

int main() {
	int age, standard;
	string first_name, last_name;

	cin >> age >> first_name >> last_name >> standard;

	Student st;
	st.set_age(age);
	st.set_standard(standard);
	st.set_first_name(first_name);
	st.set_last_name(last_name);

	cout << st.get_age() << "\n";
	cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
	cout << st.get_standard() << "\n";
	cout << "\n";
	cout << st.to_string();

	return 0;
}
